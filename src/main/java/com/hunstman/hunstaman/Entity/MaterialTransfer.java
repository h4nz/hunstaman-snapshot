package com.hunstman.hunstaman.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MaterialTransfer {
    private String id;
    private String desc;
    private Double actualAmount;
    private String unit;
}
