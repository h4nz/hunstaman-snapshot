package com.hunstman.hunstaman.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document(collection = "mixingoutput")
public class MixingOutput extends ProcessOrder {

}
