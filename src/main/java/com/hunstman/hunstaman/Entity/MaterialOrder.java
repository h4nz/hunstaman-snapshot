package com.hunstman.hunstaman.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@Document(collection = "materialOrder")
public class MaterialOrder {
    @Id
    private String processOrderNumber;
    private String materialOrderNumber;
    private String materialDesc;
    private String batchNumber;
    private String date;
    private String createdBy;
    private List<Material> material;
}
