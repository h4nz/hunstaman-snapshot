package com.hunstman.hunstaman.Entity;

import lombok.Data;

@Data
public class User {
    private String userName;
    private String password;
    private boolean valid;
}
