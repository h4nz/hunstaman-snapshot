package com.hunstman.hunstaman.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@Document(collection = "transferOrder")
public class TransferOrder {
    @Id
    private String transferOrderNumber;
    private String processOrderNumber;
    private String materialOrderNumber;
    private String materialDesc;
    private String batchNumber;
    private String valutadate;
    private String approvedBy;
    private List<MaterialTransfer> material;
}
