package com.hunstman.hunstaman.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document(collection = "porcessOrder")
public class ProcessOrder {
    @Id
    private String processOrderNumber;
    private String materialOrderNumber;
    private String materialDesc;
    private String unit;
    private Integer qty;
    private Double unitAmount;
    private Double totalAmount;
}
