package com.hunstman.hunstaman.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Material {
    private String id;
    private String desc;
    private Double amount;
    private String unit;
}
