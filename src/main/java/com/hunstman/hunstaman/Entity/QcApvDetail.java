package com.hunstman.hunstaman.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class QcApvDetail {
    private String unit;
    private Integer qty;
    private Double unitAmount;
    private Double totalAmount;
}
