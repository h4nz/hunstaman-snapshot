package com.hunstman.hunstaman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HunstamanApplication {

	public static void main(String[] args) {
		SpringApplication.run(HunstamanApplication.class, args);
	}

}
