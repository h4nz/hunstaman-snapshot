package com.hunstman.hunstaman.Repo;

import com.hunstman.hunstaman.Entity.ProcessOrder;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PoRepo extends MongoRepository<ProcessOrder,String> {

}
