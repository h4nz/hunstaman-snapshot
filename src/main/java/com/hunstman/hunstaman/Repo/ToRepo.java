package com.hunstman.hunstaman.Repo;

import com.hunstman.hunstaman.Entity.TransferOrder;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToRepo extends MongoRepository<TransferOrder,String> {

}
