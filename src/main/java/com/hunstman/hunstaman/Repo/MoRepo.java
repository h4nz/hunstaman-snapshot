package com.hunstman.hunstaman.Repo;

import com.hunstman.hunstaman.Entity.MixingOutput;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoRepo extends MongoRepository<MixingOutput,String> {

}
