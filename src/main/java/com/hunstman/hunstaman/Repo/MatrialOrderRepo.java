package com.hunstman.hunstaman.Repo;

import com.hunstman.hunstaman.Entity.MaterialOrder;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatrialOrderRepo extends MongoRepository<MaterialOrder,String> {

}
