package com.hunstman.hunstaman.Repo;

import com.hunstman.hunstaman.Entity.QcApproval;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QcApvRepo extends MongoRepository<QcApproval,String> {
}
