package com.hunstman.hunstaman.Controller;

import com.hunstman.hunstaman.Entity.*;
import com.hunstman.hunstaman.Repo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/demo")
@CrossOrigin
public class DemoController {

    @Autowired
    PoRepo poRepo;

    @Autowired
    MatrialOrderRepo materialOrderRepo;

    @Autowired
    ToRepo toRepo;

    @Autowired
    MoRepo moRepo;

    @Autowired
    QcApvRepo qcApvRepo;

    @CrossOrigin
    @RequestMapping(value = "/user/all",method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAllUser() throws Exception {
        try{
            List<User> userList = createdUser();
            return ResponseEntity.ok().body(userList);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/user/valid",method = RequestMethod.POST)
    public ResponseEntity<User> userValid(@RequestBody User user) throws Exception {
        try{
            User userResponse = new User();
            userResponse.setUserName(user.getUserName());
            userResponse.setPassword(user.getPassword());
            userResponse.setValid(false);

            List<User> userList = createdUser();

            for (User userSearch : userList) {
                if (userSearch.getUserName().equals(user.getUserName()) && userSearch.getPassword().equals(user.getPassword()) ) {
                    userResponse.setValid(true);
                }
            }

            return ResponseEntity.ok().body(userResponse);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/po",method = RequestMethod.POST)
    public ResponseEntity<ProcessOrder> submitProcessOrder(@RequestBody ProcessOrder order) throws Exception {
        try{
            this.poRepo.save(order);
            return ResponseEntity.ok().body(order);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/po/processOrderNumber",method = RequestMethod.GET)
    public ResponseEntity<ProcessOrder> getById(@RequestParam String orderNumber) throws Exception {
        try{
            ProcessOrder processOrder = new ProcessOrder();
            Optional<ProcessOrder> optional = this.poRepo.findById(orderNumber);
            if(optional.isPresent()){
                processOrder = optional.get();
            }else{
                processOrder.setProcessOrderNumber("-");
                processOrder.setMaterialOrderNumber("-");
            }

            return ResponseEntity.ok().body(processOrder);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }


    @CrossOrigin
    @RequestMapping(value = "/material",method = RequestMethod.POST)
    public ResponseEntity<MaterialOrder> materialOrder(@RequestBody MaterialOrder materialOrder) throws Exception {
        try{
            this.materialOrderRepo.save(materialOrder);
            return ResponseEntity.ok().body(materialOrder);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/material/processOrderNumber",method = RequestMethod.GET)
    public ResponseEntity<MaterialOrder> materialOrderFindByPoNumber(@RequestParam String processOrderNumber) throws Exception {
        try{
            MaterialOrder moResponse = new MaterialOrder();
            Optional<MaterialOrder> optional = this.materialOrderRepo.findById(processOrderNumber);
            if(optional.isPresent()){
                moResponse = optional.get();
            }else{
                moResponse.setProcessOrderNumber("-");
                moResponse.setMaterialOrderNumber("-");
                moResponse.setBatchNumber("-");
            }

            return ResponseEntity.ok().body(moResponse);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/materials",method = RequestMethod.GET)
    public ResponseEntity< List <MaterialOrder>> materialOrderFindByPoNumber() throws Exception {
        try{
            return ResponseEntity.ok().body(this.materialOrderRepo.findAll());
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/transferOrder",method = RequestMethod.POST)
    public ResponseEntity<TransferOrder> transferOrder(@RequestBody TransferOrder transferOrderReq) throws Exception {
        try{
            this.toRepo.save(transferOrderReq);
            return ResponseEntity.ok().body(transferOrderReq);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
    @CrossOrigin
    @RequestMapping(value = "/transferorder",method = RequestMethod.GET)
    public ResponseEntity<List<TransferOrder>> getToAll() throws Exception {
        try{
            return ResponseEntity.ok().body(this.toRepo.findAll());
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }


    @CrossOrigin
    @RequestMapping(value = "/transferorder/transferordernumber",method = RequestMethod.GET)
    public ResponseEntity<TransferOrder> transferOrderByToNumber(@RequestParam String toNumber) throws Exception {
        try{
            TransferOrder toResponse = new TransferOrder();
            Optional<TransferOrder> optional = this.toRepo.findById(toNumber);
            if(optional.isPresent()){
                toResponse = optional.get();
            }else{
                toResponse.setProcessOrderNumber("-");
                toResponse.setMaterialOrderNumber("-");
                toResponse.setBatchNumber("-");
                toResponse.setTransferOrderNumber("-");
            }

            return ResponseEntity.ok().body(toResponse);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }

    }

    @CrossOrigin
    @RequestMapping(value = "/mixingout",method = RequestMethod.POST)
    public ResponseEntity<MixingOutput> submitMixingOutPut(@RequestBody MixingOutput order) throws Exception {
        try{
            this.moRepo.save(order);
            return ResponseEntity.ok().body(order);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/mixingout/processOrderNumber",method = RequestMethod.GET)
    public ResponseEntity<MixingOutput> getMixingOutPutById(@RequestParam String orderNumber) throws Exception {
        try{
            MixingOutput moResponse = new MixingOutput();
            Optional<MixingOutput> optional = this.moRepo.findById(orderNumber);
            if(optional.isPresent()){
                moResponse = optional.get();
            }else{
                moResponse.setProcessOrderNumber("-");
                moResponse.setMaterialOrderNumber("-");
            }

            return ResponseEntity.ok().body(moResponse);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/mixingouts",method = RequestMethod.GET)
    public ResponseEntity<List<MixingOutput>> getAllMo() throws Exception {
        try{

            return ResponseEntity.ok().body(this.moRepo.findAll());
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/qcapv",method = RequestMethod.POST)
    public ResponseEntity<QcApproval> submitQcApproval(@RequestBody QcApproval order) throws Exception {
        try{
            this.qcApvRepo.save(order);
            return ResponseEntity.ok().body(order);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }



    private List<User> createdUser(){
        List<User> userList = new ArrayList<>();

        User user1 = new User();
        user1.setUserName("adminPo");
        user1.setPassword("123456");
        userList.add(user1);
        User user2 = new User();
        user2.setUserName("operator");
        user2.setPassword("123456");
        userList.add(user2);
        User user3 = new User();
        user3.setUserName("spvMaterial");
        user3.setPassword("123456");
        userList.add(user3);
        User user4 = new User();
        user4.setUserName("spvMixing");
        user4.setPassword("123456");
        userList.add(user4);
        User user5 = new User();
        user5.setUserName("adminQc");
        user5.setPassword("123456");
        userList.add(user5);
        return userList;
    }

}
